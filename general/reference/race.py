import os
import random
import time

runnerPos = [0, 0, 0, 0]
raceFinished = False

while not raceFinished:
    # Set interval
    time.sleep(.25)
    os.system('cls') if 'nt' in os.name else os.system('clear')

    # Loop through the list, 50% chance to increase the positions runnerPos[index] += 1
    for x in range(0, 4):
        if random.randint(0, 1) == 0:
            runnerPos[x] += 1
        print('-' * runnerPos[x] + 'R' + '-' * (20 - runnerPos[x]))

    # Display winner
    if max(runnerPos) >= 20:
        print(f'The winner is #{(runnerPos.index(20)) + 1}')
        break
