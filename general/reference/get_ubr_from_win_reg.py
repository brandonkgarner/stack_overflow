# https://stackoverflow.com/questions/64586304/full-os-build-number/64587895#64587895

import winreg

# Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\UBR
a = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, r'SOFTWARE\Microsoft\Windows NT\CurrentVersion')
t = winreg.QueryValueEx(a, "UBR")[0]
a.Close()

print(t)