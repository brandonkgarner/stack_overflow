# https://stackoverflow.com/questions/64921077/how-can-i-get-the-correct-ip-address-via-python/64921427#64921427

import socket


def get_internal_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    ip = s.getsockname()[0]
    s.close()
    return ip


def get_external_ip():
    return socket.gethostbyname(socket.gethostname())


print('Internal:', get_internal_ip())
print('External:', get_external_ip())
