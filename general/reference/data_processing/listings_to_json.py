import json
import re


def process_data(data, data_format='json'):
    if data_format not in ['json', 'dict']:
        raise ValueError

    prices = {}
    # For each group between '=' separators
    for x in re.split(r'[=]+', data.replace(':', '').replace('\n', ' ')):
        # For each group of words
        for d in range(len(x.split())):
            values = {}
            for e in range(1, len(x.split())):
                # For every other word
                if e % 2 == 1:
                    # Set key:value
                    values[str(f'{x.strip().split(" ")[e]}')] = str(f'{x.strip().split(" ")[e + 1]}')
            # Add above key:values to prices dict named first word in word group
            prices[f'{x.strip().split(" ")[0]}'] = values

    if 'json' in data_format:
        return json.dumps(prices)
    else:
        return prices  # python dict


ss = """AMAZON
IPHONE: 700
SAMSUNG: 600

=============

WALMART
IPHONE: 699

===========

ALIBABA
SONY: 500"""

print(process_data(ss, data_format='dict'))
print(process_data(ss))
