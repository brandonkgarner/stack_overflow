x = 4


def say_it(val):
    st = 'Our number is '
    for pos, ch in enumerate(st):
        if pos != len(st) - 1:
            print(ch, end="")
        else:
            print("", val)


print('Our number is ' + str(x))
print('Our number is %s' % x)
print('Our number is {}'.format(x))
print(f'Our number is {x}')
print('Our number is X'.replace('X', str(x)))
say_it(x)