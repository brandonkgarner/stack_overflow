import pyperclip as pyperclip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome()
driver.get("https://www.google.com/")


def copy_and_paste(element):
    key_phrase = 'This is a test'

    if len(key_phrase) > 10:
        pyperclip.copy(key_phrase)
        element.send_keys(Keys.CONTROL, 'v')


# Copy/paste to element
copy_and_paste(driver.find_element(By.NAME, "q"))

# Assert first result has expected text
first_result = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//div[contains(@class,'sbl1')]/span")))
assert 'This is a test'.lower() in first_result.get_attribute('innerHTML').lower()

driver.quit()
