import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.get("https://www.google.com/")

search = driver.find_element(By.NAME, "q")
# search = driver.find_element_by_name("q")
search.send_keys("test")

assert "Google" in driver.page_source

driver.quit()
