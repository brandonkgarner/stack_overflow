from os import listdir
folder_name = "csv_data"
files = listdir(folder_name)

data = []
for file in files:
    individualFiles = open(f"{folder_name}/{file}", "r")
    data.append(list(individualFiles.readlines()))

print(data)
