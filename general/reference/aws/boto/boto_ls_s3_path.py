import boto3

s3 = boto3.client("s3")
response = s3.list_objects_v2(
    Bucket='test',
    Delimiter='/',
    Prefix='folder/',
    MaxKeys=100)

for x in response['Contents']:
    print(x['Key'])