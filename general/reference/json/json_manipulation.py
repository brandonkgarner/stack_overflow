import json

with open('data.json', 'r') as f:
    x = json.loads(f.read())

x['data']['array'][0]['domain'] = x.pop('key').pop('domain')

with open('dataout.json', 'w') as f:
    f.write(json.dumps(x, indent=4))
