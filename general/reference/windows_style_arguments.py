# https://stackoverflow.com/questions/64812922/is-it-possible-to-use-windows-style-command-line-options-with-python/64813480#64813480

import sys

# List of switches to watch for
switches = ['test', 'blah', 'g']

if __name__ == '__main__':
    for arg in sys.argv:
        if arg[0] == '/':
            arg = arg[1:]  # Remove /
            # Look for value
            a1, v1 = arg.split(':') if ':' in arg else (arg, None)  # Separate arg:value

            # parse for whole words/letters that take values
            if a1 in switches:
                print(f'caught {a1}')
                if v1 is not None:
                    print(f'^ and it\'s value {v1}')
            elif a1 not in switches and v1 is not None:
                print(f'Option {a1} does not support values ({v1})')
            else:
                # If not words (with/without values) or letters that support values
                # look at each letter individually
                for op in range(len(arg)):
                    # Do whatever for switches
                    print(f'caught {arg[op]}')

# Run with
# python windows_style_arguments.py /fa /test:big /g:vals /h:ttt